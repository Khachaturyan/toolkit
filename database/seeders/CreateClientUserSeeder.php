<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

class CreateClientUserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $role = Role::create(['name' => 'Client']);
        $permissions = Permission::where('name', 'statement-create')->pluck('id', 'id')->all();

        $role->syncPermissions($permissions);
    }
}
