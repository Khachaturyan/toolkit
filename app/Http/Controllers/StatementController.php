<?php

namespace App\Http\Controllers;

use App\Models\Statement;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;

class StatementController extends Controller
{

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'title' => 'required',
            'description' => 'required',
            'number' => 'required|int',
            'file' => 'mimes:jpg,bmp,png,jpeg'
        ]);

        if ($validator->fails()) {
            return $this->getFailResponse("validation_fail", $validator->getMessageBag()->getMessages());
        }

        $path = Storage::putFile('/', $request->file('file'));

        $statement = Statement::create([
            'title' => $request->title,
            'description' => $request->description,
            'number' => $request->number,
            'file' => "app/{$path}"
        ]);

        return $this->getSuccessResponse('done', $statement);
    }

    public function index(){
        return $this->getSuccessResponse('done',Statement::all());
    }

    /**
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function show($id){
        $validator = Validator::make(['id' => $id], [
            'id' => 'exists:statements,id'
        ]);
        if ($validator->fails()) {
            return $this->getFailResponse("validation_fail", $validator->getMessageBag()->getMessages());
        }
        return $this->getSuccessResponse('done', Statement::find($id));
    }

    /**
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make(['id' => $id], [
            'id' => 'exists:statements,id'
        ]);
        if ($validator->fails()) {
            return $this->getFailResponse("validation_fail", $validator->getMessageBag()->getMessages());
        }
        $statement = Statement::find($id);

        if ($statement->update($request->all()) === false) {
            return $this->getFailResponse('something went wrong,statement does`t update');
        }

        return $this->getSuccessResponse('done', $statement);
    }

    /**
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function delete($id)
    {
        $validator = Validator::make(['id' => $id], [
            'id' => 'exists:statements,id'
        ]);
        if ($validator->fails()) {
            return $this->getFailResponse("validation_fail", $validator->getMessageBag()->getMessages());
        }
        Statement::find($id)->delete();

        return $this->getSuccessResponse('done');
    }
}
