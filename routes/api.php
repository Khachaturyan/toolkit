<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/


Route::post('/token', 'App\Http\Controllers\TokenController@store');

Route::middleware(['auth:sanctum'])->group(function () {
    Route::middleware(['role:Admin'])->group(function(){
        Route::get('/users', 'App\Http\Controllers\UserController@index');
        Route::post('/users','App\Http\Controllers\UserController@store');
        Route::get('/users/{id}','App\Http\Controllers\UserController@show');
        Route::patch('/users/{id}','App\Http\Controllers\UserController@update');
        Route::delete('/users/{id}','App\Http\Controllers\UserController@delete');

        Route::get('/statements','App\Http\Controllers\StatementController@index');
        Route::get('/statements/{id}','App\Http\Controllers\StatementController@show');
        Route::patch('/statements/{id}','App\Http\Controllers\StatementController@update');
        Route::delete('/statements/{id}','App\Http\Controllers\StatementController@delete');
    });
    Route::middleware(['permission:statement-create'])->group(function(){
        Route::post('/statements','App\Http\Controllers\StatementController@store');
    });
});
