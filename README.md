#for installing...

Clone this repo

````
git clone https://gitlab.com/Khachaturyan/toolkit
````


install all dependencies
``composer install``

copy .env.example as .env

add to .env file 
````
SWAGGER_VERSION=2.0
SESSION_DOMAIN=localhost
SANCTUM_STATEFUL_DOMAINS=localhost
 ````

setup your db connection credentials in .env
````
DB_CONNECTION=yourConnection
DB_HOST=yourHost
DB_PORT=yourPort
DB_DATABASE=yourDatabaseName
DB_USERNAME=yourDatabaseUserName
DB_PASSWORD=yourPassword
````

to migrate db tables run command

```
php artisan migrate
```

for seeding roles, permissions and admin users run command

```
php artisan db:seed
``` 

for testing run
````
php artisan serv
```` 

Swagger docs in path 
``"/api/documentation"``
